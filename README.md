# My containers...

## Run 1
```
docker build .
docker run -p 3000:3000 container_id
```

## Run 2
```
docker build --build-arg APP_BASE_PREFIX=/prefix/name/ .
docker run -p 3000:3000 container_id
```

## Run 3
```
docker build .
docker run -e "APP_BASE_PREFIX=/prefix/name/" -p 3000:3000 container_id
```